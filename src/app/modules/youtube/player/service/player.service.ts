import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { appConfig } from 'appConfig';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  constructor(private http:HttpClient) { 

  }
  checkVideo(ID:string){
    const params: any = {
      id  : ID,
      part: appConfig.partsToLoad+",contentDetails,player,statistics,status",
      key : appConfig.youtubeApiKey,
    };
    return this.http.get<any>(appConfig.getYoutubeEndPoint('videos'), {params});
  }
}
