import { TestBed,inject,async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { PlayerService } from './player.service';
/**
 * I wrote this
 */
describe('PlayerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
    ],
    providers: [
      PlayerService,
    ],
  }));

  it(`should create`, async(inject([HttpTestingController, PlayerService],
    (httpClient: HttpTestingController, apiService: PlayerService) => {
      expect(apiService).toBeTruthy();
  })));
});
