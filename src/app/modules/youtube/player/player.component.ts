import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { appConfig } from 'appConfig';
import { PlayerService } from './service/player.service';

@Component({
  selector   : 'app-player',
  templateUrl: './player.component.html',
  styleUrls  : [ './player.component.scss' ]
})
export class PlayerComponent implements OnInit {
  public embedUrl: any;
  public videoLoader: boolean;

  constructor(
    private sanitizer: DomSanitizer,
    private playerservice: PlayerService,
    private router:Router
  ){}

  public ngOnInit() {
    const ID = window.location.href
                     .replace(/^.*\//g, '')
                     .replace(/^.*\..*/g, '');
    if (!ID.length) {
      return;
    }
    
    /**
     * here I added relocate when video link is wrong
     */
    this.playerservice.checkVideo(ID).subscribe(data=>{
      if( data.pageInfo.totalResults <= 0 ){
        this.router.navigate(['/']);
      }
    })


    this.videoLoader = true;
    /**
     * here I fixed video url
     */
    this.embedUrl = this.sanitizer.bypassSecurityTrustResourceUrl(appConfig.getYoutubeEmbdedUrl(ID));
  }

  /* On video ready hide loader */
  public loadVideo(): void {
    this.videoLoader = true;
  }

}
