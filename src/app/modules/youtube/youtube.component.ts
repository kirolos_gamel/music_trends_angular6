import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';
import { throwError } from 'rxjs/index';

import { YoutubeService } from './service/youtube.service';
import { ContextService } from '@shared/context.service';
import { VideoClass } from './models/video.class';
import { appConfig } from 'appConfig';

@Component({
  selector   : 'app-youtube-component',
  templateUrl: './youtube.component.html',
  styleUrls  : [ './youtube.component.scss' ]
})

export class YoutubeComponent implements OnInit {
  public trendingVideos: Observable<VideoClass[]>;
  public loadingError$ = new Subject<boolean>();
  public videos: VideoClass[] = [];
  infiniteLoad:boolean = false;
  pageToken:string;
  maxvideoslength:number;
  finishedVideos:boolean = false;


  constructor(private youtubeService: YoutubeService,
              private appContext: ContextService) {
  }


  /**
   * here I reconstruct load videos function
   */
  public ngOnInit(): void {
    this.appContext.moduleTitle.next('YOUTUBE');
    this.loadVideos();
    this.appContext.videosCountPerPage.subscribe((count) => this.loadVideos(count));
  }

  private loadVideos(videosPerPage?: number,token?:string) {
    this.infiniteLoad = true;
    this.finishedVideos = false;
    if(videosPerPage && (videosPerPage > 50)){
      
      this.videos = [];
      let counter = Math.floor(videosPerPage / 50);
      let remain = videosPerPage % 50;
      let counterArray = [];
      for(let i = 0; i < counter; i ++){
        counterArray.push(50);
        if((i == (counter - 1)) && remain != 0){
          counterArray.push(remain);
        }
      }
      counterArray.map(counteritem => {
        this.getLoadedVideos(counteritem,this.pageToken);
      });
    }else{
      if(!token){
        this.videos = [];
      }

      this.getLoadedVideos(videosPerPage,token);
    }
  }

  getLoadedVideos(vcount,vpagetoken){
    this.trendingVideos = this.youtubeService.getTrendingVideos(vcount,vpagetoken)
      .pipe(
        catchError((error: any) => {
          this.loadingError$.next(true);
          return throwError(error);
        })
      );
        this.trendingVideos.subscribe((data:any)=>{
          this.infiniteLoad = false;
          this.pageToken = data.nextPageToken;
          this.maxvideoslength = data.pageInfo.totalResults;
          data.items.map((item) => {
            this.videos.push(new VideoClass(item));
          });
      })
  }
  /**
   * I added infinite scroll function
   */
  onScroll(){
    if( this.videos.length >= this.maxvideoslength ){
      this.finishedVideos = true;
      return
    }
    this.loadVideos(25,this.pageToken);
  }
  

}
