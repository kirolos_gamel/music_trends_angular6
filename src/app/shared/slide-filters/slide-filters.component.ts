import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

import { appConfig } from 'appConfig';
import { ICountryListModel } from '@shared/models/country-list.interface';
import { ICategoryListInterface } from '@shared/models/category-list.interface';
import { ContextService } from '@shared/context.service';

@Component({
  selector   : 'app-slide-filters',
  templateUrl: './slide-filters.component.html',
  styleUrls  : [ './slide-filters.component.scss' ]
})
export class SlideFiltersComponent implements OnInit {
  @Input() public filterSlideClose: any;
  public countryFormControl: FormControl = new FormControl();
  public countryList: ICountryListModel[] = appConfig.countryList;
  filteredOptions: Observable<ICountryListModel[]>;

  public categoryFormControl: FormControl = new FormControl();
  public categoriesList: ICategoryListInterface[] = [
    {name: 'Music', id: 10}
  ];

  public defaultVideosOnPage: number = appConfig.maxVideosToLoad;

  constructor(private appContext: ContextService) {
  }

  public ngOnInit() {
    /**
     * here I added material autocoplate countries filter
     */
    this.filteredOptions = this.countryFormControl.valueChanges
      .pipe(
        startWith<string | ICountryListModel>(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(name => name ? this._filter(name) : this.countryList.slice())
      );
    this.setDefaults();
  }

  /**
   * start part of countries mat autocomplate
   */
  countryDisplayFn(country?: ICategoryListInterface): string | undefined {
    return country ? country.name : undefined;
  }
  private _filter(value: string): ICountryListModel[] {
    const filterValue = value.toLowerCase();

    return this.countryList.filter(country => country.name.toLowerCase().indexOf(filterValue) === 0);
  }

  viewByCountry(event){
    appConfig.defaultRegion = event.code;
    this.appContext.videosCountPerPage.next(appConfig.maxVideosToLoad);
  }
  /**
   * end part of countries mat autocomplate
   */
  
  viewByCategory(event){
    /**
     * I fixed category search filter
     */
    const categoryId = this.categoriesList.find((cat) => cat.name === event).id;
    appConfig.defaultCategoryId = categoryId;
    this.appContext.videosCountPerPage.next(appConfig.maxVideosToLoad);
  }
  
  private setDefaults() {
    const defaultCountry = this.countryList.find((country) =>
      country.code === appConfig.defaultRegion).name;
    const defaultCategory = this.categoriesList.find((country) =>
      country.id === appConfig.defaultCategoryId).name;

    this.countryFormControl.setValue(defaultCountry);
    this.categoryFormControl.setValue(defaultCategory);
  }
}
