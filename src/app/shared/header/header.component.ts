import { Component, Input, HostListener } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Router, Event, NavigationEnd } from '@angular/router';

import { ContextService } from '../context.service';

@Component({
  selector   : 'app-page-header',
  templateUrl: './header.component.html',
  styleUrls  : [ './header.component.scss' ]
})
export class HeaderComponent {
  @Input() public filterSlideOpen: any;
  /**
   * when filter open then scroll is to top
   */
  @HostListener('click')
  click(){
    document.querySelector('.inf_scr_class').scroll(0,0);
  }
  public title$: Subject<string> = this.appContext.moduleTitle;
  public settBTN:boolean = true;

  constructor(private appContext: ContextService, router:Router) {
    /**
     * here I added route for filter button hidden
     */
    router.events.subscribe((event:Event)=>{
      if( event instanceof NavigationEnd ){
        if( (event.url === "/") || (event.url === "/youtube") ){
          this.settBTN = true;
        }else{
          this.settBTN = false;
        }
      }
    })
  }
  

}
